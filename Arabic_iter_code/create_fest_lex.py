#Create Festival compiled lexicon format from Sphinx lexicon

#Usage: python create_fest_lex.py SPHINXDIC FESTLEX
#Output is in FESTLEX file

import sys

DIC = open(sys.argv[1], "r")
dic = DIC.read()
DIC.close()

FESTLEX = open(sys.argv[2], "w")
FESTLEX.close()

FESTLEX = open(sys.argv[2], "a")
FESTLEX.write("MNCL\n")
FESTLEX.close()

for line in dic.split("\n")[:-1]:
	word = line.split()[0].replace("(", "").replace(")", "")
	ph = " ".join(line.split()[1:])
	FESTLEX =  open(sys.argv[2], "a")
	FESTLEX.write("(\""+word+"\" nil ((("+ph+") 0 )))\n")
	#assume we don't have syllable or stress information
	FESTLEX.close()
