#filter lexicon to only include base words and variants that are in tdd
#do this for voice building so that words can be included in the lexicon file withut exceeding the stack limit
#Usage python filt_lex.py LEX TDD OUTLEX

import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python  filt_lex.py LEX TDD OUTLEX")
        sys.exit()

LEX = open(sys.argv[1], "r")
lex = LEX.read()
LEX.close()

TDD = open(sys.argv[2], "r")
tdd = TDD.read()
TDD.close()

OUTLEX = open(sys.argv[3], "w")
OUTLEX.close()

wordlist = []

for line in tdd.split("\n")[:-1]:
	wordlist.extend(line.split()[2:])

uniqwords = list(set(wordlist))
#print(uniqwords)

for line in lex.split("\n")[:-1]:
	if line.find("lex.add.entry") != -1:
		lexwordstart = line.find("\"")
		lexwordend = (line[lexwordstart+1:]).find("\"")
		lexword = line[lexwordstart+1:lexwordstart+1+lexwordend]
		if lexword.replace("a", "").replace("b", "").replace("c", "").replace("d", "").replace("e", "").replace("f", "").replace("g", "").replace("h", "") in uniqwords:
		        OUTLEX = open(sys.argv[3], "a")
	                OUTLEX.write(line+"\n")
        	        OUTLEX.close()

	else:
		OUTLEX = open(sys.argv[3], "a")
		OUTLEX.write(line+"\n")
		OUTLEX.close()
