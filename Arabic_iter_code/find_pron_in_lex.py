#Script to find force aligned pronunciations in the festival lexicon so it does not complain while building prompts
#Run this script after running maketdd..py

#Usage python maketdd.py LEX ORIGTDD NEWTDD 
#Missing words are printed out

import sys

if len(sys.argv) != 4:
	print("Error: must provide 3 arguments")
	print("Usage: python find_pron_in_lex.py LEX ORIGTDD NEWTDD")
	sys.exit()

ORIG = open(sys.argv[2], "r")
orig = ORIG.read()
ORIG.close()

LEX = open(sys.argv[1], "r")
lex = LEX.read()
LEX.close()

NEWTDD = open(sys.argv[3], "r")
newtdd = NEWTDD.read()
NEWTDD.close()

lex_list = []

for lex_line in lex.split("\n")[1:-1]:
	lexword = lex_line.split()[0][2:-1]
        lex_list.append(lexword)


for orig_line in orig.split("\n")[:-1]:
	orig_tokens = orig_line.split()[2:-1]
	for tok in orig_tokens:
		tok = tok.replace("\"", "")
		if tok  not in lex_list:
			print("Missing word: "+tok)
