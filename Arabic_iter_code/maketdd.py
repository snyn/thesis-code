#Script to create new txt.done.data file using the original txt.done.data file and new force-aligned transcript
#We use the old file because Sphinx may not force-align all the sentences
#In such cases, we use the original transcript and fall back on the default pronunciations

#Usage python maketdd.py ORIGTDD FORCEALIGN NEWTDD 
#Output will be in NEWTDD file

import sys

if len(sys.argv) != 4:
	print("Error: must provide 3 arguments")
	print("Usage: python maketdd.py ORIGTDD FORCEALIGN NEWTDD")
	sys.exit()

ORIG = open(sys.argv[1], "r")
orig = ORIG.read()
ORIG.close()

FA = open(sys.argv[2], "r")
fa = FA.read()
FA.close()

NEWTDD = open(sys.argv[3], "w")
NEWTDD.close()

for orig_line in orig.split("\n")[:-1]:
	orig_fileid = orig_line.split()[1]
	found = 0
	for fa_line in fa.split("\n")[:-1]:
		fa_sent = " ".join(fa_line.split()[1:-2]) #get rid of beginning and end silence and file id
		fa_fileid = fa_line.split()[-1][1:-1]
		fa_sent = fa_sent.replace("(", "").replace(")", "").replace("<sil>", "")  #because sphinx puts brackets around the number
		if orig_fileid == fa_fileid:
			found = 1
			NEWTDD = open(sys.argv[3], "a")
			NEWTDD.write("( "+fa_fileid+" \""+fa_sent+"\" )\n")
			NEWTDD.close()
			break
	if found == 0:
		NEWTDD = open(sys.argv[3], "a")
                NEWTDD.write(orig_line+"\n")
                NEWTDD.close()
