#Map all words in dictionary and txt.done.data into ascci
#so that there arent any utf8explode issues and missing words at build_prompts time

#Usage python map_dic_to_ascii.py LEX TDD NEWLEX NEWTDD
#Mapped words are replaced in NEWLEX and NEWTDD which can then be used in place of the originals

import sys
from itertools import product
from string import ascii_lowercase

if len(sys.argv) != 5:
        print("Error: must provide 4 arguments")
        print("Usage: python map_dic_to_ascii.py LEX TDD NEWLEX NEWTDD")
        sys.exit()

LEX = open(sys.argv[1], "r")
lex = LEX.read()
LEX.close()

TDD = open(sys.argv[2], "r")
tdd = TDD.read()
TDD.close()

NEWLEX = open(sys.argv[3], "w")
NEWLEX.close()

NEWTDD = open(sys.argv[4], "w")
NEWTDD.close()

#dictionary containing lexical entry and mapped ascii characters
lex_map_dic = {}

#first decide how many ascii letters needed for all words
#hard coding to 4 for now
#do this automatically later
NUM = 4
ascii_words = [''.join(i) for i in product(ascii_lowercase, repeat = NUM)]
ascii_words_lex = ascii_words[:len(lex.split("\n"))]

#first read all the lexical entries
for lex_line, ascii_word in zip(lex.split("\n")[1:-1], ascii_words_lex):
	lex_word = lex_line.split()[0][2:-1]
	lex_map_dic[lex_word] = ascii_word

for lex_line in lex.split("\n")[1:-1]:
	NEWLEX = open(sys.argv[3], "a")
	lex_word = lex_line.split()[0][2:-1]
	NEWLEX.write(lex_line.replace(lex_word, lex_map_dic[lex_word])+"\n")
	NEWLEX.close()

for tdd_line in tdd.split("\n")[:-1]:
	tdd_sent = tdd_line.split()[2:-1]
	tdd_mapped_sent = ""
	for tdd_word in tdd_sent:
		tdd_word = tdd_word.replace("\"", "")
		try:
			tdd_mapped_word = lex_map_dic[tdd_word]
		except KeyError:
			print("Word not found in dictionary: "+tdd_word)
			tdd_mapped_word = ""
		tdd_mapped_sent = tdd_mapped_sent+tdd_mapped_word+" "
	tdd_fileid = tdd_line.split()[1]
	NEWTDD = open(sys.argv[4], "a")
	NEWTDD.write("( "+tdd_fileid+" \""+tdd_mapped_sent+"\" )\n")
	NEWTDD.close()
		

