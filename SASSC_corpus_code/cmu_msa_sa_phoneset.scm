;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                     ;;;
;;;                     Carnegie Mellon University                      ;;;
;;;                  and Alan W Black and Kevin Lenzo                   ;;;
;;;                      Copyright (c) 1998-2000                        ;;;
;;;                        All Rights Reserved.                         ;;;
;;;                                                                     ;;;
;;; Permission is hereby granted, free of charge, to use and distribute ;;;
;;; this software and its documentation without restriction, including  ;;;
;;; without limitation the rights to use, copy, modify, merge, publish, ;;;
;;; distribute, sublicense, and/or sell copies of this work, and to     ;;;
;;; permit persons to whom this work is furnished to do so, subject to  ;;;
;;; the following conditions:                                           ;;;
;;;  1. The code must retain the above copyright notice, this list of   ;;;
;;;     conditions and the following disclaimer.                        ;;;
;;;  2. Any modifications must be clearly marked as such.               ;;;
;;;  3. Original authors' names are not deleted.                        ;;;
;;;  4. The authors' names are not used to endorse or promote products  ;;;
;;;     derived from this software without specific prior written       ;;;
;;;     permission.                                                     ;;;
;;;                                                                     ;;;
;;; CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK        ;;;
;;; DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING     ;;;
;;; ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT  ;;;
;;; SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE     ;;;
;;; FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES   ;;;
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN  ;;;
;;; AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,         ;;;
;;; ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF      ;;;
;;; THIS SOFTWARE.                                                      ;;;
;;;                                                                     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Phonset for cmu_msa
;;;

;;;  Feeel free to add new feature values, or new features to this
;;;  list to make it more appropriate to your language

(defPhoneSet
  cmu_msa
  ;;;  Phone Features
  (;; vowel or consonant
   (clst + - 0)
   (vc + - 0)
   ;; vowel length: short long dipthong schwa
   (vlng s l d a 0)
   ;; vowel height: high mid low
   (vheight 1 2 3 0 -)
   ;; vowel frontness: front mid back
   (vfront 1 2 3 0 -)
   ;; lip rounding
   (vrnd + - 0)
   ;; consonant type: stop fricative affricative nasal lmsauid approximant
   (ctype s f a n l r 0)
   ;; place of articulation: labial alveolar palatal
   ;; labio-dental dental velar glottal
   (cplace l a p b d v g 0)
   ;; consonant voicing
   (cvox + - 0)
   (asp  + - 0)
   (nuk + - 0)   


   )
  (
   
;; this is not case sensitive so that Sphinx can deal with it
;; some of the feature values are made up but most of them should be correct
;; some of the phones may not appear in the data and may need to be removed
   
   (pau - - 0 - - - 0 0 0 0 0)  ;; silence ... 
   ;(DA - + l 1 1 - 0 0 0 0 0)
   ;(SD - 0 0 0 0 0 0 0 0 - -)
   ;(HB - - 0 0 0 0 s g - - +)
   ;(HA - - 0 0 0 0 s g - + +)
   ;(MA - + 0 0 0 0 s g + + -)
   ;(LH - - 0 0 0 0 s g + - -)
   ;(HY - - 0 0 0 0 s g - + -)
   ;(SW - - 0 0 0 0 f p - - -) ;;changed from SH
   ;(DW - - 0 0 0 0 f d + - -) ;;changed from DH
   ;(HW - - 0 0 0 0 s g + - +)
   (AH - + a 2 2 - 0 0 0 0 0)
   (A - - 0 0 0 0 s g - - -)
   (AA - - 0 0 0 0 l g - - -) ;wrong
   (AZ - - 0 0 0 0 s a - - -) ;wrong
   (AS - - 0 0 0 0 s g - - +) ;wrong
   (AAS - - 0 0 0 0 l g - - +) ;wrong


   (B - - 0 0 0 0 s l + - -)
   (D - - 0 0 0 0 s a + - -)
   (DS - - 0 0 0 0 s a + - +)

   ;(DH - - 0 0 0 0 s a + - +) 
   ;(E - - 0 0 0 0 f g + - +)
   (F - - 0 0 0 0 f d - - -)
   ;(FH - - 0 0 0 0 f d - - +)
   ;(G - - 0 0 0 0 f v + - -)
   (GH - - 0 0 0 0 f v + - +)
   (H - - 0 0 0 0 f g - - -)
   ;(HH - - 0 0 0 0 f g - - +)
   (I - + s 1 1 - 0 0 0 0 0) 
   (IS - + s 1 1 - 0 0 0 0 +) ;wrong

   (J - - 0 0 0 0 s v + - -)
   (JU - - 0 0 0 0 s v + - 0) ;wrong
   (JN - - 0 0 0 0 s v + - +) ;wrong

   ;(CH - - 0 0 0 0 a p - - -)
   (K - - 0 0 0 0 s v - - -)
   (KX - - 0 0 0 0 s v - - +)
   (L - - 0 0 0 0 r a + - -)
   (LS - - 0 0 0 0 r a + - +);wrong


   (M - - 0 0 0 0 n l + - -)
   (N - - 0 0 0 0 n a + - -)
   ;(NN - - 0 0 0 0 n a + - +)
   ;(O - + d 3 2 - 0 0 0 0 0)
   ;(TM - + a 2 2 - 0 0 0 0 +)
   ;(P - - 0 0 0 0 s l - - -)
   (Q - - 0 0 0 0 s v - - +)
   (R - - 0 0 0 0 r a + - -)
   (RS - - 0 0 0 0 r a + - +);wrong


   (S - - 0 0 0 0 f a - - -)
   (SS - - 0 0 0 0 f a - - +);wrong

   (SH - - 0 0 0 0 f a - - +)
   (TH - - 0 0 0 0 s a - - +)
   (T - - 0 0 0 0 s a - - -)
   (THS - - 0 0 0 0 s a - - 0) ;wrong
   (TS - - 0 0 0 0 s a - - +) ;wrong



   (U - + s 1 3 + 0 0 0 0 0)
   (US - + s 1 3 + 0 0 0 0 +) ;wrong

   ;(V - - 0 0 0 0 f d - - -)
   (W - - 0 0 0 0 r l + - -)
   ;(X - - 0 0 0 0 f v - - -)
   (Y - - 0 0 0 0 a p + - -)
   ;(AM - + l 1 1 - 0 0 0 0 +)
   (Z - - 0 0 0 0 f a + - -)
   ;(ZH - - 0 0 0 0 f a + - +)
  )
)


(PhoneSet.silences '(pau))

(define (cmu_msa_sa::select_phoneset)
  "(cmu_msa_sa::select_phoneset)
Set up phone set for cmu_msa."
  (Parameter.set 'PhoneSet 'cmu_msa)
  (PhoneSet.select 'cmu_msa)
)

(define (cmu_msa_sa::reset_phoneset)
  "(cmu_msa_sa::reset_phoneset)
Reset phone set for cmu_msa."
  t
)

(provide 'cmu_msa_sa_phoneset)                                      
