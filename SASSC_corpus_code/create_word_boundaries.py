#create intermediate files in PARTLAB folder that contain word boundary information and phoneme names extracted from FULLLAB files
#Usage python create_word_boundaries.py FULLLAB PARTLAB

import sys
import os

if len(sys.argv) != 3:
        print("Error: must provide 3 arguments")
        print("Usage: python create_word_boundaries.py FULLLAB PARTLAB")
        sys.exit()

os.system("mkdir "+sys.argv[2])

fullab_files = os.listdir(sys.argv[1])
fullab = sys.argv[1]
partlab = sys.argv[2]

for f in fullab_files:
	#create output file
	OUT = open(partlab+"/"+f, "w")
	OUT.close()
	INP = open(fullab+"/"+f, "r")
	inp = INP.read()
	INP.close()
	phlist = []
	poswordlist = []
	posphrlist = []
	for line in inp.split("\n")[:-1]:
		#print(line)
		ph = line[line.find("-")+1:line.find("+")]
		#print(ph)
		poswordpart = line[line.find("E:"):]
		poswordpart2 = poswordpart[poswordpart.find("@")+1:]
		posword = poswordpart2[:poswordpart2.find("+")]
		#print(posword)
		posphrpart = line[line.find("H:"):]
		posphrpart2 = posphrpart[posphrpart.find("@")+1:]
		posphr = posphrpart2[:posphrpart2.find("=")]
		#print(posphr)
		phlist.append(ph)
		poswordlist.append(posword)
		posphrlist.append(posphr)
	#print(phlist)
	#print(poswordlist)
	#print(posphrlist)
	#ignore first and last entries in all list, they correspond to beginning and end pauses
	prevw = "1"
	prevphr = "1"
	for p, w, r in zip(phlist[1:-1], poswordlist[1:-1], posphrlist[1:-1]):
		#print(p+" "+w+" "+r)
		if p != "pau":
			currphr = r
			currw = w
			if currphr != prevphr:
				#start new phrase
				#print("new phrase starting")
				#os.system("cp partlab/"+f+" multphr/"+f)
				prevw = "0"
				#print(f)
				#print(p+" "+w+" "+r)
			#else:
			#	print("still in old phrase")
			if currw != prevw:
			#	print("starting new word")
				#start new word, add a # between
				OUT = open(partlab+"/"+f, "a")
				OUT.write("#")
				OUT.close()
               		OUT = open(partlab+"/"+f, "a")
                	OUT.write(" "+p+" ")
                	OUT.close()
			prevw = currw
			prevphr = currphr


