#Read in lexicon and label it with multiple entries
#Usage python find_mult_entries.py LEX NEWLEX

import sys

if len(sys.argv) != 3:
        print("Error: must provide 2 arguments")
        print("Usage: python find_mult_entries.py LEX NEWLEX")
        sys.exit()

INPLEX = open(sys.argv[1], "r")
inplex = INPLEX.read()
INPLEX.close()

words = []

OUT = open(sys.argv[2], "w")
OUT.close()


for line in inplex.split("\n")[:-1]:
        OUT = open(sys.argv[2], "a")
        word = line.split(" ")[-1]
        #dont remove syllable marker from phones (for now)
        phones = " ".join(line.split(" ")[:-1])
        #pos = line.split("\t")[2]
        if word in words:
                newword = "xxx"
                count = 1
                while newword in words:
                        count = count+1
                        #print(count)
                        newword = word+" ("+str(count)+")"
                #print(newword+"\t"+phones+"\n")
                words.append(newword)
                OUT.write(newword+"\t"+phones+"\n")
        else:
                words.append(word)
                #print("writing"+word)
                OUT.write(word+"\t"+phones+"\n")
        OUT.close()
