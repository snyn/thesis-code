# -*- coding: utf-8 -*-

#fix al problem in sorted_lex and create final festival lexicon
# look for ﺎﻠْ at beginning of word and insert an l
# look for l at end of word and remove l
# format is ("ﺄﺑﺎﺘﺸﻳ" NPROP (((HA AX B A T SW IH Y) 0 ) ))
# Also map the phonemes that are the same except case 
#Usage: python fix_al_and_map.py LEX NEWLEX PHONEME-MAP


############# TO DO ###############
#Use phoneme map file and make number replacement better

import os
import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python fix_al_and_map.py LEX NEWLEX PHONEME-MAP")
        sys.exit()


INP = open(sys.argv[1], "r")
inp = INP.read()
INP.close()

OUT = open(sys.argv[2], "w")
OUT.close()

MAP = open(sys.argv[3], "r")
ph_map = MAP.read()
MAP.close()

for line in inp.split("\n")[:-1]:
	#print(line)
	ar_word = line.split("\t")[0]
	phonemes = line.split("\t")[1]
	#print(ar_word)
	#print(phonemes)
	#print(line)
	ar_word_base = ar_word.replace(")", "").replace("(", "").replace("1", "").replace("2", "").replace("3", "").replace("4", "").replace("5", "").replace("6", "").replace("7", "").replace("8", "").replace("9", "").replace("10", "").replace("11", "").replace("12", "").replace("13", "").replace("14", "").replace("15", "")
	if ar_word_base[:2]  == "ﺎﻠْ" and phonemes[-2:] != "l":
		print(ar_word+" has the determiner")
		newphonemes = "l "+phonemes
	else:
		newphonemes = phonemes
	if phonemes[-2:] == "l " and ar_word_base[:2]  != "ﺎﻠْ":
		print("l found at end of word "+ar_word)
		newphonemes = phonemes[:-2]
	else:
		newphonemes = phonemes
	#now map all phonemes
	#should use file but it isnt much 
	ph_mapped = newphonemes.replace(" a ", " AS ").replace(" aa ", " AAS ").replace(" d ", " DS ").replace(" i ", " IS ").replace(" l ", " LS ").replace(" r ", " RS ").replace(" s ", " SS ").replace(" t ", " TS ").replace(" th ", " THS ").replace(" u ", " US ").upper()
	OUT = open(sys.argv[2], "a") 
	OUT.write("(\""+ar_word.replace(")", "").replace("(", "").replace(" ", "")+"\" ((("+ph_mapped+") 0 )))\n")
	OUT.close()
