# -*- coding: utf-8 -*-
#Use intermediate file and transcript files and create a lexicon 
#Usage: python makelex.py PATH-TO-PARTLAB PATH-TO-TRANS LEX

import os
import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python makelex.py PATH-TO-PARTLAB PATH-TO-TRANS LEX")
        sys.exit()


lab = os.listdir(sys.argv[1])
labpath = sys.argv[1]

transpath = sys.argv[2]
FULLEX = open(sys.argv[3], "w")
FULLEX.close()

for f in lab:
	INPLAB = open(labpath+"/"+f, "r")
	inplab = INPLAB.read()
	INPLAB.close()
	try:
		INPTR = open(transpath+"/"+f[:-3]+"txt", "r")
		inptr = INPTR.read()
		INPTR.close()
	except IOError:
		print(f+" not found")
		continue
		#os.system("rm partlab"+f)
	inplab = inplab.replace("# pau", "")
	inptr = inptr.replace(",", "")[:-1].replace(".", "").replace("،", "").replace("؟", "").replace(":", "").replace("}", "").replace("{", "").replace("-", "").replace("\"", "").replace("  ", " ").replace("  ", " ").replace("  ", " ").replace("  ", " ").replace("  ", " ").replace("  ", " ")

	if inptr[0] == " ":
		inptr = inptr[1:]
	#print("aligning")
	#print(inplab)
	#print(inptr)
        #add rule here to shift l in word boundary if ﺍappears in the first letter of the word
	#newinp = ""
	#for l, tr in zip(inplab.split("#")[:-1], inptr.split(" ")):
	#	if tr[1] == "ﺍ":
	#		print("determiner")
	#		l = "l "+ l
	#	newinp = newinp + l
	#print(newinp, tr)	
		#newinp = newinp + prevl 

	for l, tr in zip(inplab.split("#"), inptr.split(" ")):
		
		FULLEX = open(sys.argv[3], "a")
		#print(l[1:]+" "+tr+"\n")
		FULLEX.write(l[1:]+" "+tr+"\n")
		FULLEX.close()
