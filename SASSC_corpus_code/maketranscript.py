#Make ASR and TTS transcripts from SASSC corpus data
#Usage python make_transcript.py PATH-TO-TTS-TEXT PATH-TO-ASR-TEXT FESTFILE SPHFILE
#Assumes you have all the files you want in your TTS and ASR transcripts in two directories
#specify None in path or festfile or sphfile if you only want ASR or TTS transcripts
import sys
import os

if len(sys.argv) != 5:
        print("Error: must provide 4 arguments")
        print("python make_transcript.py PATH-TO-TTS-TEXT PATH-TO-ASR-TEXT FESTFILE SPHFILE")
        sys.exit()

if sys.argv[1] != "None" or sys.argv[3] != "None":
        tts_files = os.listdir(sys.argv[1])
	tts_file_path = sys.argv[1]
        FESTFILE = open(sys.argv[3], "w")
        FESTFILE.close()
        for f in tts_files:
                F = open(tts_file_path+"/"+f, "r")
                text = F.read().split("\n")[0]
                F.close()
                FESTFILE = open(sys.argv[3], "a")
                FESTFILE.write("( "+f[:-4]+" \""+text+"\" )\n")
                FESTFILE.close()
else:
        print("Not creating TTS transcript")


if sys.argv[2] != "None" or sys.argv[4] != "None":
	asr_files = os.listdir(sys.argv[2])
	asr_file_path = sys.argv[2]
	SPHFILE = open(sys.argv[4], "w")
	SPHFILE.close()
	for f in asr_files:
		F = open(asr_file_path+"/"+f, "r")
		text = F.read().split("\n")[0]
		F.close()
		SPHFILE = open(sys.argv[4], "a")
		SPHFILE.write("<s> "+text+" </s> ("+f[:-4]+")\n")
		SPHFILE.close()
else:
	print("Not creating ASR transcript")
