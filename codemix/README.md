This folder contains scripts for codemixing as described in the Synthesizing Code Mixed Text chapter in my thesis

There are two main parts: first, process a large corpus to help with spelling normalization. Then, process the text.done.data file (that you may have created using the larger corpus). An optional third part is mapping the phoneme sets of the two languages automatically.

Assumptions:

1. For now we assume that the code mixing is between English and another language but this can be extended

2. We also assume that the script is the same for both languages being mixed (otherwise you can use the script to figure out what the different languages are)

3. This code includes SoundEx for Indic languages and English, but you can add other implementations to do_soundex.py

4. Mapping between the two phoneme sets can be done manually or automatically.

Steps:

1. Processing the large code mixed corpus

process_corpus.py <CORPUS> <CLUSTERS>

2. Processing the txt.done.data file

process_tdd.py <CLUSTERS> <TDD> <NEWTDD>

3. Map phoneme sets

map_phoneme_sets.py <PHONESET-SRC> <PHONESET-TGT> <MAPPING> 
