#map phones from Hindi to English based on phonetic feature distance
#Using weights from Sriram et al. ICON 2014

#Usage python map_phones.py SRC-PHNSET TGT-PHNSET MAPFILE
#Source and target phonesets should be in Festvox *_phoneset.scm format and have phonetic features in right order
#for now only give it the phonemes with phonetic features (see example files) and remove pau, brth etc phonemes

import sys
import os
import operator

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python map_phones.py SRC-PHNSET TGT-PHNSET MAPFILE")
        sys.exit()

SRCPH = open(sys.argv[1], "r")
srcph = SRCPH.read()
SRCPH.close()

TGTPH = open(sys.argv[2], "r")
tgtph = TGTPH.read()
TGTPH.close()

OUTMAP = open(sys.argv[3], "w")
OUTMAP.close()

feat_weights = {'clst':0.5, 'vc':7, 'vlng':0.5, 'vheight':5, 'vfront':5, 'vrnd':5, 'ctype':4, 'cplace':4, 'cvox':1, 'asp':0.5, 'nuk':0.5}
feat_weights_list = [0.5, 7, 0.5, 5, 5, 5, 4, 4, 1, 0.5, 0.5] 
src_phones = {}
tgt_phones = {}
src_tgt_map = {}

#read both files and make feat vectors, should collapse this into a single function
for line in srcph.split("\n")[:-1]:
	#print line
	comment = line.find(";") #to remove stuff after comment
	line = line[:comment]
	line = line.replace("(", "").replace(")", "")
	tok = line.split()
	#print(tok)
	#print(len(tok))
	if len(tok) < 12:
		add = 12-len(tok)-1
		tok = [tok[0]]+[0]+tok[1:]+(add*[0])
        phone_name = tok[0]
        src_phones[phone_name] = tok[1:]


for line in tgtph.split("\n")[:-1]:
	#print line
        comment = line.find(";") #to remove stuff after comment
        line = line[:comment]
        line = line.replace("(", "").replace(")", "")
        tok = line.split()
	#print(tok)
        #print(len(tok))
        #make sure that all phonesets have 12 phonetic features and make them 0 if they don't
	if len(tok) < 12:
                add = 12-len(tok)-1 #because clst is in the beginning, this is hacky do it better
                tok = [tok[0]]+[0]+tok[1:]+(add*[0])
	phone_name = tok[0]
	tgt_phones[phone_name] = tok[1:]
	#print(tok)

#print(src_phones)
#print(tgt_phones)
score_tgt = {}
#mapping source (Hin) to target (Eng) phones so go through list of all Hindi phones
for ph_src in src_phones:
	for ph_tgt in tgt_phones:
		src_feats = src_phones[ph_src]
		tgt_feats = tgt_phones[ph_tgt]	
		feat_count = 0 #should have designed the feat weight vector better
		score = 0 #score for this particular target phone
		for f1, f2 in zip(src_feats, tgt_feats):
			if f1 == f2 and f1 != 0:
				#print "match"
				score = score+feat_weights_list[feat_count]
			feat_count = feat_count+1
		score_tgt[ph_tgt] = score
	#print(score_tgt)
	#print("Best match for "+ph_src)	
	best_match = max(score_tgt.iteritems(), key=operator.itemgetter(1))[0]
	#haven't figured out what to do if there are multiple best matches, maybe play with weights so no two weights are exactly same
	#print(best_match)
	OUTMAP = open(sys.argv[3], "a")
	OUTMAP.write(ph_src+" "+best_match+"\n")
	OUTMAP.close()

