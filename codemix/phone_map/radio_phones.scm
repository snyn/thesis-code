   (aa  +   l   3   3   -   0   0   0) ;; father
   (ae  +   s   3   1   -   0   0   0) ;; fat
   (ah  +   s   2   2   -   0   0   0) ;; but
   (ao  +   l   3   3   +   0   0   0) ;; lawn
   (aw  +   d   3   2   -   0   0   0) ;; how
   (ax  +   a   2   2   -   0   0   0) ;; about
   (axr +   a   2   2   -   r   a   +)
   (ay  +   d   3   2   -   0   0   0) ;; hide
   (b   -   0   0   0   0   s   l   +)
   (ch  -   0   0   0   0   a   p   -)
   (d   -   0   0   0   0   s   a   +)
   (dh  -   0   0   0   0   f   d   +)
   (dx  -   a   0   0   0   s   a   +) ;; ??
   (eh  +   s   2   1   -   0   0   0) ;; get
   (el  +   s   0   0   0   l   a   +)
   (em  +   s   0   0   0   n   l   +)
   (en  +   s   0   0   0   n   a   +)
   (er  +   a   2   2   -   r   0   0) ;; always followed by r (er-r == axr)
   (ey  +   d   2   1   -   0   0   0) ;; gate
   (f   -   0   0   0   0   f   b   -)
   (g   -   0   0   0   0   s   v   +)
   (hh  -   0   0   0   0   f   g   -)
   (hv  -   0   0   0   0   f   g   +)
   (ih  +   s   1   1   -   0   0   0) ;; bit
   (iy  +   l   1   1   -   0   0   0) ;; beet
   (jh  -   0   0   0   0   a   p   +)
   (k   -   0   0   0   0   s   v   -)
   (l   -   0   0   0   0   l   a   +)
   (m   -   0   0   0   0   n   l   +)
   (n   -   0   0   0   0   n   a   +)
   (nx  -   0   0   0   0   n   d   +) ;; ???
   (ng  -   0   0   0   0   n   v   +)
   (ow  +   d   2   3   +   0   0   0) ;; lone
   (oy  +   d   2   3   +   0   0   0) ;; toy
   (p   -   0   0   0   0   s   l   -)
   (r   -   0   0   0   0   r   a   +)
   (s   -   0   0   0   0   f   a   -)
   (sh  -   0   0   0   0   f   p   -)
   (t   -   0   0   0   0   s   a   -)
   (th  -   0   0   0   0   f   d   -)
   (uh  +   s   1   3   +   0   0   0) ;; full
   (uw  +   l   1   3   +   0   0   0) ;; fool
   (v   -   0   0   0   0   f   b   +)
   (w   -   0   0   0   0   r   l   +)
   (y   -   0   0   0   0   r   p   +)
   (z   -   0   0   0   0   f   a   +)
   (zh  -   0   0   0   0   f   p   +)
