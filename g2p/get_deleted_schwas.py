#Read txt.phseq.data from unitran voice
#look at label and decide if schwas should be deleted or not
#Look for label against all phonemes that are followed by 'A'
#Usage python get_delted_schwas.py PHSEQ PRED-DIR NEWPHSEQ

import sys
import os

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python get_delted_schwas.py PHSEQ PRED-DIR NEWPHSEQ")
        sys.exit()

PHSEQ = open(sys.argv[1], "r")
phseq = PHSEQ.read()
PHSEQ.close()

pred_files = os.listdir(sys.argv[2])
pred_files_path = sys.argv[2]

NEWPHSEQ = open(sys.argv[3], "w")
NEWPHSEQ.close()

for line in phseq.split("\n")[:-1]:
	fid = line.split()[0]
	phones = line.split()[1:]
	try:
		PREDFILE = open(pred_files_path+"/hin_"+fid+".data.pred", "r")
		predfile = PREDFILE.read()
		PREDFILE.close()
	except IOError:
		print("File not found: hin_"+fid)
		continue
	count = 0
	for ph, phline in zip(phones, predfile.split("\n")[:-1]):
		print("ph: "+ph)
		print("phline: "+phline)
		count = count+1
		try:
			if phones[count] == "A":
				print("This phone is followed by schwa")
				if phline == "0":
					NEWPHSEQ = open(sys.argv[3], "a")
					NEWPHSEQ.write(ph+"* ")
					NEWPHSEQ.close()
				else:
                                        NEWPHSEQ = open(sys.argv[3], "a")
                                        NEWPHSEQ.write(ph+" ")
                                        NEWPHSEQ.close()
			else:
				NEWPHSEQ = open(sys.argv[3], "a")
                                NEWPHSEQ.write(ph+" ")
                                NEWPHSEQ.close()
                except:
                        print("end of line")

	NEWPHSEQ = open(sys.argv[3], "a")
        NEWPHSEQ.write("\n")
        NEWPHSEQ.close()
