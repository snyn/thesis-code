#get durations and confidence values of each phone in .phseg files for each sentence in the force-aligned file
#Run this after force-alignment is run
#Files with dur and ASR confidence for each phone will be in DATAFILES-DIR
#Usage python get_dur_conf.py PHSEG-DIR FALIGN DATAFILES-DIR
import os
import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python get_dur_conf.py PHSEG-DIR FALIGN DATAFILES-DIR")
        sys.exit()

phlist = os.listdir(sys.argv[1])

FALIGN = open(sys.argv[2], "r")
falign = FALIGN.read()
FALIGN.close()

phsegpath = sys.argv[1]
datafilespath = sys.argv[3]

for f in phlist:
	fid = f[:-6]
	#print(fid)
	PHSFILE = open(phsegpath+"/"+f, "r")
	phsfile = PHSFILE.read()
	PHSFILE.close
	for l in falign.split("\n")[:-1]:
		fileid = l.split()[-1][1:-1]
		if fileid == fid:
			#print(l)
			break
	OUT = open(datafilespath+"/"+fid+".data", "w")
	OUT.close()
	count = 0
	for line in phsfile.split("\n")[1:-2]:
		count = count + 1
		for i in range(1, 10):
			line = line.replace("  ", " ")
                line = line.replace("\t ", " ")
		#print(line)
		phoneme = line.split(" ")[4]
		conf = line.split(" ")[3]
		durstart = line.split(" ")[1]
		durend = line.split(" ")[2]
		#print(phoneme)
		#print(conf)
		dur = str(eval(durend) - eval(durstart))
		#print(dur)
		OUT = open(datafilespath+"/"+fid+".data", "a")
		OUT.write(phoneme+" "+dur+" "+conf+"\n")
		OUT.close()
		
