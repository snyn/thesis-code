#Convert prompt file from Festival to Sphinx format and back
#Rearranges stuff and ads or gets rid of brackets around numbers
#Usage python conv_fest_sph.py to_fest FESTFILE SPHFILE
#Or python conv_fest_sph.py to_sph SPHFILE FESTFILE

import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage (sph-to-fest): conv_fest_sph.py to_fest SPHFILE FESTFILE")
	print("(fest-to-sph): python conv_fest_sph.py to_sph FESTFILE SPHFILE")
        sys.exit()

if sys.argv[1] == "to_fest":
	print("Convert to fest")
	SPHFILE = open(sys.argv[2], "r")
	sphfile = SPHFILE.read()
	SPHFILE.close()
	FESTFILE = open(sys.argv[3], "w")
	FESTFILE.close()
	for line in sphfile.split("\n")[:-1]:
		fid = line.split()[-1][1:-1]
		sent = " ".join(line.split()[1:-2]).replace("(", "").replace("<sil>", "").replace("  ", " ")
		FESTFILE = open(sys.argv[3], "a")
		FESTFILE.write("( "+fid+" \""+sent+"\" )\n")
		FESTFILE.close()
else:
	if sys.argv[1] == "to_sph":
		print("Convert to sph")
		FESTFILE = open(sys.argv[2], "r")
        	festfile = FESTFILE.read()
        	FESTFILE.close()
        	SPHFILE = open(sys.argv[3], "w")
        	SPHFILE.close()
	        for line in festfile.split("\n")[:-1]:
	                fid = line.split()[1]
	                sent = " ".join(line.split()[2:-1]).replace("\"", "")
	    	        SPHFILE = open(sys.argv[3], "a")
			#add brackets for numbers, only till 9, add more if needed
			for num in range(2, 10):
				sent.replace(str(num), "("+str(num)+")")
			SPHFILE.write("<s> "+sent+" </s> ("+fid+")\n")
        	        SPHFILE.close()
	else:
		print("Error. Must specify to_fest or to_sph")
		sys.exit()

