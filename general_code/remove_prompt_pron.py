#Remove prompt that has word with no entry in dictionary
#Do this only with your final dictionary and make sure entry wasn't there in original dic
#And that entries haven't disappeared when you processed the dictionary
#Usage python remove_prompt_pron.py PROMPTFILE DIC NEWPROMPTFILE
#Output is in NEWPROMPTFILE

import sys

if len(sys.argv) != 4:
        print("Error: must provide 3 arguments")
        print("Usage: python remove_prompt_pron.py PROMPTFILE DIC NEWPROMPTFILE")
        sys.exit()


PRFILE = open(sys.argv[1], "r")
prfile = PRFILE.read()
PRFILE.close()

DIC = open(sys.argv[2], "r")
dic = DIC.read()
DIC.close()

NEWPRFILE = open(sys.argv[3], "w")
NEWPRFILE.close()

dic_words = []

for dic_line in dic.split("\n")[:-1]:
	dic_word = dic_line.split()[0]
	dic_words.append(dic_word)

for pr_line in prfile.split("\n")[:-1]:
	flag = 1
	for w in pr_line.split()[1:-2]:
		if w not in dic_words:
			#print(w+" not found")
			flag = 0
			break
	if flag == 1:
		NEWPRFILE = open(sys.argv[3], "a")
		NEWPRFILE.write(pr_line+"\n")
		NEWPRFILE.close()
