import sys

fi = open(sys.argv[1], 'r')
name = sys.argv[2]
phones = set()

for li in fi:
	ll = li.split()[:-1]
	for ph in ll:
		phones.add(ph)
ofi = open(name+".phone", 'w')
for ph in phones:
	ofi.write(ph+'\n')
ofi.close()
ofi = open(name+".dic", 'w')
for ph in phones:
	ofi.write(ph+'\t'+ph+'\n')
ofi.close()
